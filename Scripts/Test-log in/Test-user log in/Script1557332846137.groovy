import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl('http://18.212.33.201:8081/')

WebUI.setText(findTestObject('Page_ProjectBackend/input_Username_username'), 'user')

WebUI.setEncryptedText(findTestObject('Page_ProjectBackend/input_Password_password'), '1/VWEm4uipk=')

WebUI.click(findTestObject('Page_ProjectBackend/button_Login'))

WebUI.verifyElementPresent(findTestObject('Page_ProjectBackend/warptext_Product'), 0)

WebUI.verifyElementPresent(findTestObject('Page_ProjectBackend/warptext_Carts'), 0)

WebUI.closeBrowser()


import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl('http://18.212.33.201:8081/')

WebUI.setText(findTestObject('Page_ProjectBackend/input_Username_username'), 'user')

WebUI.setEncryptedText(findTestObject('Page_ProjectBackend/input_Password_password'), '1/VWEm4uipk=')

WebUI.click(findTestObject('Page_ProjectBackend/button_Login'))

WebUI.click(findTestObject('Page_ProjectBackend/button_add to cart_Garden'))

WebUI.click(findTestObject('Page_ProjectBackend/button_add to cart_banana'))

WebUI.click(findTestObject('Page_ProjectBackend/button_add to cart_orange'))

WebUI.click(findTestObject('Page_ProjectBackend/button_add to cart_Papaya'))

WebUI.click(findTestObject('Page_ProjectBackend/button_add to cart_Rambutan'))

WebUI.verifyElementPresent(findTestObject('Page_ProjectBackend/button_already added_Garden'), 0)

WebUI.verifyElementNotClickable(findTestObject('Page_ProjectBackend/button_already added_Garden'), FailureHandling.STOP_ON_FAILURE)

WebUI.verifyElementText(findTestObject('Page_ProjectBackend/button_already added_Garden'), 'already added')

WebUI.verifyElementNotClickable(findTestObject('Page_ProjectBackend/button_already added_banana'))

WebUI.verifyElementText(findTestObject('Page_ProjectBackend/button_already added_banana'), 'already added')

WebUI.verifyElementPresent(findTestObject('Page_ProjectBackend/button_already added_banana'), 0)

WebUI.verifyElementText(findTestObject('Page_ProjectBackend/button_already added_Orange'), 'already added')

WebUI.verifyElementNotClickable(findTestObject('Page_ProjectBackend/button_already added_Orange'), FailureHandling.STOP_ON_FAILURE)

WebUI.verifyElementNotClickable(findTestObject('Page_ProjectBackend/button_already added_Orange'))

WebUI.verifyElementPresent(findTestObject('Page_ProjectBackend/button_already added_papaya'), 0)

WebUI.verifyElementText(findTestObject('Page_ProjectBackend/button_already added_papaya'), 'already added')

WebUI.verifyElementNotClickable(findTestObject('Page_ProjectBackend/button_already added_papaya'))

WebUI.verifyElementPresent(findTestObject('Page_ProjectBackend/button_already added_Rambutan'), 0)

WebUI.verifyElementText(findTestObject('Page_ProjectBackend/button_already added_Rambutan'), 'already added')

WebUI.verifyElementNotClickable(findTestObject('Page_ProjectBackend/button_already added_Rambutan'))

WebUI.closeBrowser()

